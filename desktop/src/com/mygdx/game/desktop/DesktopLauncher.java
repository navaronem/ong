package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.OrcsNGoblins;

/**
 *
 * @author Alexander Hövelmann
 */

public class DesktopLauncher {
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "ONG";
        cfg.width = 800;
        cfg.height = 800;
        //cfg.fullscreen = true;
        
        new LwjglApplication(new OrcsNGoblins(), cfg);
    }
}
