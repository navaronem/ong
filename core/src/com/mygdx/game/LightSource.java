package com.mygdx.game;

import com.badlogic.gdx.math.Vector3;

/**
 *
 * @author Marvin Bongiovi
 */
public interface LightSource {
    public abstract Position getPosition();
    public abstract float getBrightness();
    public abstract float getMaxDistance();
    public abstract int getRayNumber();
    public abstract Vector3 getTransformedPosition();
}
