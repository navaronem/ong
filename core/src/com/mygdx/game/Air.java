/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 *
 * @author Marvin Bongiovi
 */
public class Air extends Tile{
    public static final Sprite TEXTURE = new Sprite(new Texture(Gdx.files.internal("empty_air.png")), Tile.TILE_SIZE, Tile.TILE_SIZE);
    public static final Sprite TEXTURE_TOP = new Sprite(new Texture(Gdx.files.internal("empty_air.png")), Tile.TILE_SIZE, Tile.TILE_SIZE);
    
    public Air() {
        super(false, false, false, true, 1);
    }

    @Override
    public Sprite getSprite() {
        return Air.TEXTURE;
    }
    
    @Override
    public Sprite getTopSprite() {
        return Air.TEXTURE_TOP;
    }

    @Override
    public String toString() {
        return "Type: Air\n" + super.toString();
    }       
}
