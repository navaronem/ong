package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;

/**
 *
 * @author Marvin Bongiovi
 */
public class Torch extends Tile implements LightSource{
    public static final Sprite TEXTURE = new Sprite(new Texture(Gdx.files.internal("gras.jpg")), Tile.TILE_SIZE, Tile.TILE_SIZE);
    public static final Sprite TEXTURE_TOP = new Sprite(new Texture(Gdx.files.internal("gras.jpg")), Tile.TILE_SIZE, Tile.TILE_SIZE);
    private final Position pos;
    private final float brightness, maxDistance;
    private final int rayNumber;
    private final Vector3 transPos;
    
    public Torch(Position pos, float relX, float relY, float relZ, float brightness, float maxDistance, int rayNumber) {
        super(false, false, false, true, 1);
        this.pos = pos;
        this.brightness = brightness;
        this.rayNumber = rayNumber;
        this.maxDistance = maxDistance;
        this.transPos = pos.toVector3PixelTransformedPosition(relX, relY, relZ);
    }
    
    public Torch(Position pos, float brightness, float maxDistance, int rayNumber) {
        this(pos, 0.5f, 0.5f, 0.5f, brightness, maxDistance, rayNumber);
    }

    @Override
    public Sprite getSprite() {
        return Torch.TEXTURE;
    }

    @Override
    public Sprite getTopSprite() {
        return Torch.TEXTURE_TOP;
    }

    @Override
    public Position getPosition() {
        return pos;
    }

    @Override
    public float getBrightness() {
        return brightness;
    }

    @Override
    public int getRayNumber() {
        return rayNumber;
    }

    @Override
    public float getMaxDistance() {
        return maxDistance;
    }

    @Override
    public Vector3 getTransformedPosition() {
        return transPos;
    }   

    @Override
    public String toString() {
        return "Type: Torch\n" + super.toString();
    }    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.pos != null ? this.pos.hashCode() : 0);
        hash = 97 * hash + Float.floatToIntBits(this.brightness);
        hash = 97 * hash + Float.floatToIntBits(this.maxDistance);
        hash = 97 * hash + this.rayNumber;
        hash = 97 * hash + (this.transPos != null ? this.transPos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Torch other = (Torch) obj;
        if (Float.floatToIntBits(this.brightness) != Float.floatToIntBits(other.brightness)) {
            return false;
        }
        if (Float.floatToIntBits(this.maxDistance) != Float.floatToIntBits(other.maxDistance)) {
            return false;
        }
        if (this.rayNumber != other.rayNumber) {
            return false;
        }
        if (this.pos != other.pos && (this.pos == null || !this.pos.equals(other.pos))) {
            return false;
        }
        if (this.transPos != other.transPos && (this.transPos == null || !this.transPos.equals(other.transPos))) {
            return false;
        }
        return true;
    }  
}
