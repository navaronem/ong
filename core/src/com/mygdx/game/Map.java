/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.ArrayList;

/**
 *
 * @author Marvin Bongiovi
 */
public class Map{
    private Tile[][][] map;
    private Unit[][][] unitMap; //'Ich stehe hier!'-"Map"
    private ArrayList<Unit>[][][] movementMap;  //"Map" zum berechnen der Bewegungen
    public final int xDim, yDim, layers;
    private final Lighting light;
    private final Sight sight;
    
    public Map(int xDim, int yDim, int layers){
        this.xDim = xDim;
        this.yDim = yDim;
        this.layers = layers;
        map = new Tile[layers][xDim][yDim];
        unitMap = new Unit[layers][xDim][yDim];
        movementMap = new ArrayList[layers][xDim][yDim];
        
        for(int i = 0; i < layers; i++ ){
            for(int x = 0; x < xDim; x++){
                for(int y = 0; y < yDim; y++){  
                    //map
                    if(i == 2){
                        map[i][x][y] = new Air();
                    }else{
                        map[i][x][y] = new Dirt();
                    }    
                    //movementMap
                    movementMap[i][x][y] = new ArrayList(3);
                }
            }
        }
        
        map[1][1][0] = new Air();
        map[1][1][1] = new Air();
        map[1][0][3] = new Air();
        map[1][1][3] = new Air();
        map[1][3][3] = new Air();
        map[1][5][0] = new Air();
        map[1][5][1] = new Air();
        map[1][5][2] = new Torch(new Position(5, 2, 1), 100f, 10000, 60);
        map[1][5][3] = new Air();
        //map[1][5][4] = new Air();
        map[1][1][4] = new Air();
        map[1][2][5] = new Air();
        map[1][3][5] = new Air();
        map[1][4][5] = new Air();
        map[1][3][2] = new Air();
        map[1][3][1] = new Air();
        
        unitMap[2][3][4] = new Goblin(10, new Position(3, 4, 2), 10, Direction.NORTH);
        movementMap[2][3][4].add(unitMap[2][3][4]);
        
        //EXPERIMEN
        light = new Lighting(this);
        light.registerLightSource((LightSource) map[1][5][2]);
        
        sight = new Sight(this);
        sight.registerObserver(unitMap[2][3][4]);
    }
    
    public Tile getTile(int layer, int x, int y){
        return map[layer][x][y];
    }
    
    public ArrayList<Position> getTraversablePositions(){ //Untersützt bisher weder Gebäude noch Einheiten
        ArrayList<Position> positions = new ArrayList();
        for(int i = 0; i < layers; i++ ){
            for(int x = 0; x < xDim; x++){
                for(int y = 0; y < yDim; y++){
                    if(map[i][x][y].isTraversable()){
                        if(i == layers-1){
                            positions.add(new Position(x, y, i));
                        }
                        if(i != layers-1 && !map[i+1][x][y].isSolid()){
                            positions.add(new Position(x, y, i));
                        }
                    }                   
                }
            }
        }
        return positions;
    }

    public void draw(SpriteBatch batch, int layer){      
        for(int i = 0; i <= layer; i++ ){
            for(int x = 0; x < xDim; x++){
                for(int y = 0; y < yDim; y++){
                    if(i == layer){
                        batch.setColor(1f, 1f, 1f, 1f);
                        batch.draw(map[i][x][y].getTopSprite(), x*Tile.TILE_SIZE, y*Tile.TILE_SIZE);
                    }else{
                        float colorScale = (float)Math.pow(0.7, layer-i-1);
                        batch.setColor(colorScale, colorScale, 1f, 1f);
                        batch.draw(map[i][x][y].getSprite(), x*Tile.TILE_SIZE, y*Tile.TILE_SIZE);
                        
                        if(unitMap[i][x][y] != null){
                            batch.draw(unitMap[i][x][y].getSprite(), x*Tile.TILE_SIZE, y*Tile.TILE_SIZE);
                        }                      
                    }                   
                }
            }
        }
    }
    
    public void drawBuildingMode(SpriteBatch batch, int layer){
        for(int x = 0; x < xDim; x++){
            for(int y = 0; y < yDim; y++){
                batch.setColor(2f, 2f, 2f, 1f);
                batch.draw(map[layer][x][y].getTopSprite(), x*Tile.TILE_SIZE, y*Tile.TILE_SIZE);
                if(!map[layer][x][y].isSolid()){
                    batch.draw(new Sprite(new Texture(Gdx.files.internal("light.png")), Tile.TILE_SIZE, Tile.TILE_SIZE), x*Tile.TILE_SIZE, y*Tile.TILE_SIZE);
                }             
                
                batch.setColor(1f, 1f, 1f, 1f);
                batch.draw(map[layer-1][x][y].getSprite(), x*Tile.TILE_SIZE, y*Tile.TILE_SIZE);               
            }
        }       
    }

    public Tile[][][] getMap() {
        return map;
    }
    
    public Unit[][][] getUnitMap(){
        return unitMap;
    }
      
    public Lighting getLight() {
        return light;
    }   
}
