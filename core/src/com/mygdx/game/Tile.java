/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 *
 * @author Marvin Bongiovi
 */
public abstract class Tile {
    public static final int TILE_SIZE = 128;
    
    private final boolean traversable, buildable, solid, transparent;
    private final int movementCost;
    
    public Tile(boolean traversable, boolean buildable, boolean solid, boolean transparent, int movementCost) {
        this.traversable = traversable;
        this.buildable = buildable;
        this.solid = solid;
        this.transparent = transparent;
        this.movementCost = movementCost;
    }
    
    public abstract Sprite getSprite();
    public abstract Sprite getTopSprite();
       
    @Override
    public String toString(){
        return "Traversable: " + traversable + "\nBuildable: " + buildable + "\nSolid: " + solid + "\nMovement cost: " + movementCost;
    }

    public boolean isTraversable() {
        return traversable;
    }

    public boolean isBuildable() {
        return buildable;
    }

    public boolean isSolid() {
        return solid;
    }
    
    public boolean isTransparent() {
        return transparent;
    }    

    public int getMovementCost() {
        return movementCost;
    }        
}
