package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import java.util.ArrayList;

/**
 *
 * @author Marvin Bongiovi
 */
public class Lighting {
    private static final float DELTA = 1f; 
    private final Map map;
    private final ArrayList<LightSource> lightSources;
    private ArrayList<LightSource>[][][] lightSourceMap;
    //BM nur kurzzeitig public
    public float[][][] brightnessMap;
    
    public Lighting(Map map){
        this.map = map;
        this.lightSources = new ArrayList();
        calculateLightSourceMap();
        calculateBrightnessMap();
    }
    
    public void registerLightSource(LightSource light) {
        lightSources.add(light);
        calculateLightSourceMap();
        calculateBrightnessMap();
    }
    
    private void initializeLightSourceMap() {
        lightSourceMap = new ArrayList[map.layers][map.xDim][map.yDim];
        for(int z = 0; z < map.layers; z++) {
            for(int x = 0; x < map.xDim; x++) {
                for(int y = 0; y < map.yDim; y++) {
                    lightSourceMap[z][x][y] = new ArrayList();
                }
            }
        }
    }
    
    private void calculateLightSourceMap() {
        initializeLightSourceMap();
        for(int i = 0; i < lightSources.size(); i++) {
            for(int j = 0; j < lightSources.get(i).getRayNumber() / 2; j++) {
                for(int l = 0; l < lightSources.get(i).getRayNumber(); l++) {
                    for(float k = 0; k < lightSources.get(i).getMaxDistance(); k = k + DELTA) {
                        Vector3 pos = RayCluster.getCoordinate(lightSources.get(i).getTransformedPosition(), lightSources.get(i).getRayNumber(), j, l, k);
                        int xTile = (int) pos.x/Tile.TILE_SIZE;
                        int yTile = (int) pos.y/Tile.TILE_SIZE;
                        int zTile = (int) pos.z/Tile.TILE_SIZE;                      
                        if(xTile < 0 || xTile >= map.xDim || yTile < 0 || yTile >= map.yDim || zTile < 0 || zTile >= map.layers) break;
                        if(checkRayConflict(pos)) break;
                        if(map.getMap()[zTile][xTile][yTile].isTransparent()) {
                            if(!lightSourceMap[zTile][xTile][yTile].contains(lightSources.get(i))) {
                                lightSourceMap[zTile][xTile][yTile].add(lightSources.get(i));
                            }
                        }
                        else {
                            break;
                        }
                    }
                }
            }       
        }
    }
    
    private void calculateBrightnessMap() {
        this.brightnessMap = new float[map.layers][map.xDim][map.yDim];
        for(int i = 0; i < map.layers; i++) {
            for(int j = 0; j < map.xDim; j++) {
                for(int k = 0; k < map.yDim; k++) {
                    float zPos = ((float) (i * Tile.TILE_SIZE)) + (((float) Tile.TILE_SIZE) / 2f);
                    float xPos = ((float) (j * Tile.TILE_SIZE)) + (((float) Tile.TILE_SIZE) / 2f);
                    float yPos = ((float) (k * Tile.TILE_SIZE)) + (((float) Tile.TILE_SIZE) / 2f);
                    Vector3 pos = new Vector3(xPos, yPos, zPos);
                    for(int p = 0; p < lightSourceMap[i][j][k].size(); p++) {
                        float dist =  pos.dst2(lightSourceMap[i][j][k].get(p).getTransformedPosition())/(float)(Tile.TILE_SIZE * Tile.TILE_SIZE); 
                        brightnessMap[i][j][k] = brightnessMap[i][j][k] + Math.min(lightSourceMap[i][j][k].get(p).getBrightness(), lightSourceMap[i][j][k].get(p).getBrightness()/dist);
                    }
                }
            }
        }    
    }
    
    private boolean checkRayConflict(Vector3 pos) {
        float xLower = ((int)(pos.x / Tile.TILE_SIZE)) * Tile.TILE_SIZE;
        float xUpper = xLower + Tile.TILE_SIZE;
        float yLower = ((int)(pos.y / Tile.TILE_SIZE)) * Tile.TILE_SIZE;
        float yUpper = yLower + Tile.TILE_SIZE;
        float zLower = ((int)(pos.z / Tile.TILE_SIZE)) * Tile.TILE_SIZE;
        float zUpper = zLower + Tile.TILE_SIZE;
        
        int check = 0;
        
        if(pos.x < xLower + DELTA) check++;
        else if(pos.x > xUpper - DELTA) check++;
        if(pos.y < yLower + DELTA) check++;
        else if(pos.y > yUpper - DELTA) check++;
        if(pos.z < zLower + DELTA) check++;
        else if(pos.z > zUpper - DELTA) check++;
        
        return check >= 2;
    }
}
