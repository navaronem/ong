package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 *
 * @author Alexander Hövelmann
 */
@SuppressWarnings("EqualsAndHashcode")
public class Position {
    public final int x, y, z;
    
    public Position(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Position(Vector3 v3){
        this.x = (int)v3.x;
        this.y = (int)v3.y;
        this.z = (int)v3.z;
    }
    
    public Vector2 toVector2Position() {
        return new Vector2((float)x, (float)y);
    }
    
    public Vector2 toVector2PixelPosition() {
        return toVector2Position().scl((float)Tile.TILE_SIZE);
    }
    
    public Vector2 toVector2PixelTransformedPosition(float rel) {
        return toVector2PixelTransformedPosition(rel, rel);
    }
    
    public Vector2 toVector2PixelTransformedPosition(float relX, float relY) {
        return toVector2PixelPosition().add((float)Tile.TILE_SIZE * relX, 
                                            (float)Tile.TILE_SIZE * relY);
    }
    
    public Vector2 toVector2PixelCenteredPosition() {
        return toVector2PixelTransformedPosition(0.5f);
    }
    
    public Vector3 toVector3Position() {
        return new Vector3((float)x, (float)y, (float)z);
    }
    
    public Vector3 toVector3PixelPosition() {
        return toVector3Position().scl((float)Tile.TILE_SIZE);
    }
    
    public Vector3 toVector3PixelTransformedPosition(float rel) {
        return toVector3PixelTransformedPosition(rel, rel, rel);
    }
    
    public Vector3 toVector3PixelTransformedPosition(float relX, float relY, float relZ) {
        return toVector3PixelPosition().add((float)Tile.TILE_SIZE * relX, 
                                            (float)Tile.TILE_SIZE * relY,
                                            (float)Tile.TILE_SIZE * relZ);
    }
    
    public Vector3 toVector3PixelCenteredPosition() {
        return toVector3PixelTransformedPosition(0.5f);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.x;
        hash = 67 * hash + this.y;
        hash = 67 * hash + this.z;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        if (this.z != other.z) {
            return false;
        }
        return true;
    }    
    
    @Override
    public String toString() {
        return "Position{" + "x=" + x + ", y=" + y + ", z=" + z + '}';
    }      
}
