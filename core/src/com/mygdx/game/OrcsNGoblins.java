package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class OrcsNGoblins extends ApplicationAdapter implements InputProcessor{      
    public static final int CAMERA_SPEED = 16;
    public static final int CAMERA_ZOOM = Tile.TILE_SIZE;
    public static final int MAP_SIZE_X = 10;
    public static final int MAP_SIZE_Y = 10;
    public static final int MAX_ZOOM = Tile.TILE_SIZE * OrcsNGoblins.MAP_SIZE_X * 2;
    public static final int MIN_ZOOM = Tile.TILE_SIZE * 2;
    
    SpriteBatch batch, ui;
    OrthographicCamera camera;
    Map map;
    Stage stage;
    BitmapFont font;
    
    private Sprite selectionFrame;

    private boolean moveDown, moveLeft, moveRight, moveUp, scrollDown, scrollUp;
    private Position selectedPos;
    private int currentLayer = 2;

    @Override
    public void create () {
        selectionFrame = new Sprite(new Texture(Gdx.files.internal("selection.png")), Tile.TILE_SIZE, Tile.TILE_SIZE);
        font = new BitmapFont();
        batch = new SpriteBatch();
        ui = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Tile.TILE_SIZE*OrcsNGoblins.MAP_SIZE_X, Tile.TILE_SIZE*OrcsNGoblins.MAP_SIZE_Y);
        Gdx.input.setInputProcessor(this);
        map = new Map(OrcsNGoblins.MAP_SIZE_X, OrcsNGoblins.MAP_SIZE_Y, 3);      
    }

    @Override
    public void render () {
            Gdx.gl.glClearColor(0, 0, 0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            camera.update();
            batch.setProjectionMatrix(camera.combined);           
            batch.begin();
                map.draw(batch, currentLayer);
                if(selectedPos != null && selectedPos.z == currentLayer-1){  
                    selectionFrame.draw(batch);
                }
            batch.end();
            ui.begin();
                new Sprite(new Texture(Gdx.files.internal("gui.png")), 0, 0, Gdx.graphics.getWidth(), (int)(Gdx.graphics.getHeight()*0.15)).draw(ui);
                if(selectedPos != null){
                    Tile tempTile = map.getTile(selectedPos.z, selectedPos.x, selectedPos.y);
                    font.draw(ui, tempTile.toString(), (int)(Gdx.graphics.getWidth()*0.05/3.0), (int)(Gdx.graphics.getHeight()*0.15)-(int)(Gdx.graphics.getWidth()*0.05/3.0));
                }
            ui.end();
            manageInput();
    }

    @Override
    public void dispose () {
            batch.dispose();
    }
    
    @Override
    public void resize(int width, int height) {
        Vector3 position = camera.position.cpy();
        camera.setToOrtho(false, camera.viewportWidth, camera.viewportWidth*((float)Gdx.graphics.getHeight()/(float)Gdx.graphics.getWidth()));
        camera.position.set(position);
        scrollDown = false;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch(keycode){
            case Input.Keys.A:
                            if(moveRight) moveRight = false;
                            moveLeft = true;
                            break;
            case Input.Keys.D:
                            if(moveLeft) moveLeft = false;
                            moveRight = true;
                            break;
            case Input.Keys.DOWN:
                            if(moveUp) moveUp = false;
                            moveDown = true;
                            break;
            case Input.Keys.LEFT:
                            if(moveRight) moveRight = false;
                            moveLeft= true;
                            break;
            case Input.Keys.RIGHT:
                            if(moveLeft) moveLeft = false;
                            moveRight = true;
                            break;
            case Input.Keys.S:
                            if(moveUp) moveUp = false;
                            moveDown = true;
                            break;
            case Input.Keys.UP:
                            if(moveDown) moveDown = false;
                            moveUp = true;
                            break;
            case Input.Keys.W:
                            if(moveDown) moveDown = false;
                            moveUp = true;
                            break;
            case Input.Keys.PAGE_DOWN:
                            scrollDown = true;
                            break;
            case Input.Keys.PAGE_UP:
                            scrollUp = true;
                            break;     
            case Input.Keys.ESCAPE:
                            System.exit(0);
                            break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode){
            case Input.Keys.A:
                            moveLeft = false;
                            break;
            case Input.Keys.D:
                            moveRight = false;
                            break;
            case Input.Keys.DOWN:
                            moveDown = false;
                            break;
            case Input.Keys.LEFT:
                            moveLeft= false;
                            break;
            case Input.Keys.RIGHT:
                            moveRight = false;
                            break;
            case Input.Keys.S:
                            moveDown = false;
                            break;
            case Input.Keys.UP:
                            moveUp = false;
                            break;
            case Input.Keys.W:
                            moveUp = false;
                            break;
        }      
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        if(amount < 1){
            scrollDown = true;
        }else{
            scrollUp = true;
        }
        
        return true;
    }
    
    // currently unused overridden InputProcessor methods begin
    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    // currently only for presentation
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {               
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Vector3 clickedPos = camera.unproject(new Vector3(screenX, screenY, 0));
        if(button == 0) {    //Linksklick              
            if(screenY < (int)(Gdx.graphics.getHeight()*(1-0.15))){               
                clickedPos.x = (int)(clickedPos.x/Tile.TILE_SIZE);
                clickedPos.y = (int)(clickedPos.y/Tile.TILE_SIZE);
                clickedPos.z = currentLayer-1;
                selectedPos = new Position(clickedPos);
                selectionFrame.setBounds(selectedPos.x*Tile.TILE_SIZE, selectedPos.y*Tile.TILE_SIZE, Tile.TILE_SIZE, Tile.TILE_SIZE);
                System.out.println(map.getLight().brightnessMap[selectedPos.z][selectedPos.x][selectedPos.y]);
            }else {
                System.exit(0);
            }         
        }else if(button == 1) {  //Rechtsklick
            selectedPos = null;
        }       
        return true;
    }
    
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    // currently only for presentation
    @Override
    public boolean mouseMoved(int screenX, int screenY) {           
        return false;
    }
    
    private void manageInput(){
        if(moveDown){
            camera.translate(0, -OrcsNGoblins.CAMERA_SPEED, 0);
        }
        
        if(moveLeft){
            camera.translate(-OrcsNGoblins.CAMERA_SPEED, 0, 0);
        }
        
        if(moveRight){
            camera.translate(OrcsNGoblins.CAMERA_SPEED, 0, 0);          
        }
        
        if(moveUp){
            camera.translate(0, OrcsNGoblins.CAMERA_SPEED, 0);
        }               
        
        if(scrollDown && camera.viewportWidth>OrcsNGoblins.MIN_ZOOM){
            Vector3 position = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0); 
            camera.unproject(position);
            position = position.sub(camera.position);
            position.scl(0.1f);
            position.z = 0;
            position.add(camera.position);
            camera.setToOrtho(false, camera.viewportWidth-OrcsNGoblins.CAMERA_ZOOM, (camera.viewportWidth-OrcsNGoblins.CAMERA_ZOOM)*((float)Gdx.graphics.getHeight()/(float)Gdx.graphics.getWidth()));            
            camera.position.set(position);
            scrollDown = false;
        }else{
            scrollDown = false;
        }
        
        if(scrollUp && camera.viewportWidth<OrcsNGoblins.MAX_ZOOM){  
            Vector3 position = camera.position.cpy();
            camera.setToOrtho(false, camera.viewportWidth+OrcsNGoblins.CAMERA_ZOOM, (camera.viewportWidth+OrcsNGoblins.CAMERA_ZOOM)*((float)Gdx.graphics.getHeight()/(float)Gdx.graphics.getWidth()));
            camera.position.set(position);  
            scrollUp = false;
        }else{
            scrollUp = false;
        }
    }
}
