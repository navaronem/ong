/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 *
 * @author Alexander Hövelmann
 */
public class Goblin extends Unit{
    public static final Sprite TEXTURE_NORTH = new Sprite(new Texture(Gdx.files.internal("goblin_north.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    public static final Sprite TEXTURE_NORTH_EAST = new Sprite(new Texture(Gdx.files.internal("goblin_north_east.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    public static final Sprite TEXTURE_EAST = new Sprite(new Texture(Gdx.files.internal("goblin_east.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    public static final Sprite TEXTURE_SOUTH_EAST = new Sprite(new Texture(Gdx.files.internal("goblin_south_east.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    public static final Sprite TEXTURE_SOUTH = new Sprite(new Texture(Gdx.files.internal("goblin_south.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    public static final Sprite TEXTURE_SOUTH_WEST = new Sprite(new Texture(Gdx.files.internal("goblin_south_west.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    public static final Sprite TEXTURE_WEST = new Sprite(new Texture(Gdx.files.internal("goblin_west.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    public static final Sprite TEXTURE_NORTH_WEST = new Sprite(new Texture(Gdx.files.internal("goblin_north_west.png")), Unit.UNIT_SIZE, Unit.UNIT_SIZE);
    
    public Goblin(int maxHp, Position pos, int hp, Direction direction){
        super(maxHp, pos, hp, direction, 180, 120, 2 * Tile.TILE_SIZE,  0.85f);
    }
    
    @Override
    public Sprite getSprite() {
        switch(this.getDirection()){
            case NORTH : return Goblin.TEXTURE_NORTH;
            case NORTH_EAST : return Goblin.TEXTURE_NORTH_EAST;
            case EAST : return Goblin.TEXTURE_EAST;
            case SOUTH_EAST : return Goblin.TEXTURE_SOUTH_EAST;
            case SOUTH : return Goblin.TEXTURE_SOUTH;
            case SOUTH_WEST : return Goblin.TEXTURE_SOUTH_WEST;
            case WEST : return Goblin.TEXTURE_WEST;
            case NORTH_WEST : return Goblin.TEXTURE_NORTH_WEST;
            default : return Goblin.TEXTURE_SOUTH;
        }
    }    
}
