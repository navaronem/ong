package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import java.util.ArrayList;

/**
 *
 * @author Alexander Hövelmann
 */
public class Sight {
    private static final float DELTA = 1f; 
    private final Map map;
    private final ArrayList<Unit> observer;
    private ArrayList<Unit>[][][] sightMap;
    
    public Sight(Map map){
        this.map = map;
        this.observer = new ArrayList();
        calculateSightMap();
    }
    
    public void registerObserver(Unit unit) {
        observer.add(unit);
        calculateSightMap();
        
        for(int z=0; z<map.layers; z++){
            for(int x=0; x<map.xDim; x++){
                for(int y=0; y<map.yDim; y++){   
                    if(sightMap[z][x][y].size() > 0){
                        System.out.println("[" + x + ", " + y + ", " + z + "]");
                    }                    
                }
            }
        }
    }
    
    private void initializeSightMap() {
        sightMap = new ArrayList[map.layers][map.xDim][map.yDim];
        for(int z = 0; z < map.layers; z++) {
            for(int x = 0; x < map.xDim; x++) {
                for(int y = 0; y < map.yDim; y++) {
                    sightMap[z][x][y] = new ArrayList();
                }
            }
        }
    }
    
    private void calculateSightMap(){
        initializeSightMap();
        for(int i = 0; i < observer.size(); i++) {
            for(int j = 0; j < observer.get(i).getRayNumberH(); j++) {
                for(int l = 0; l < observer.get(i).getRayNumberV(); l++) {
                    for(float k = 0; k < observer.get(i).getMaxViewDistance(); k = k + DELTA) {
                        Vector3 pos = RayCluster.getViewCoordinate(observer.get(i).getTransformedPosition(), observer.get(i).getDirection().toVector2(),
                                                                   observer.get(i).getViewAngleH(), observer.get(i).getViewAngleV(), observer.get(i).getRayNumberH(), 
                                                                   observer.get(i).getRayNumberV(), j, l, k);
                        int xTile = (int) pos.x/Tile.TILE_SIZE;
                        int yTile = (int) pos.y/Tile.TILE_SIZE;
                        int zTile = (int) pos.z/Tile.TILE_SIZE;  
                        if(xTile < 0 || xTile >= map.xDim || yTile < 0 || yTile >= map.yDim || zTile < 0 || zTile >= map.layers) break;
                        if(checkRayConflict(pos)) break;
                        if(map.getMap()[zTile][xTile][yTile].isTransparent()) {
                            if(!sightMap[zTile][xTile][yTile].contains(observer.get(i))) {
                                sightMap[zTile][xTile][yTile].add(observer.get(i));
                            }
                        }
                        else {
                            break;
                        }
                    }
                }
            }       
        }
    }
    
    private boolean checkRayConflict(Vector3 pos) {
        float xLower = ((int)(pos.x / Tile.TILE_SIZE)) * Tile.TILE_SIZE;
        float xUpper = xLower + Tile.TILE_SIZE;
        float yLower = ((int)(pos.y / Tile.TILE_SIZE)) * Tile.TILE_SIZE;
        float yUpper = yLower + Tile.TILE_SIZE;
        float zLower = ((int)(pos.z / Tile.TILE_SIZE)) * Tile.TILE_SIZE;
        float zUpper = zLower + Tile.TILE_SIZE;
        
        int check = 0;
        
        if(pos.x < xLower + DELTA) check++;
        else if(pos.x > xUpper - DELTA) check++;
        if(pos.y < yLower + DELTA) check++;
        else if(pos.y > yUpper - DELTA) check++;
        if(pos.z < zLower + DELTA) check++;
        else if(pos.z > zUpper - DELTA) check++;
        
        return check >= 2;
    }
}
