/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import java.util.ArrayList;

/**
 *
 * @author Alexander Hövelmann
 */
public class Dijkstra {
    private int[] distance;
    private Position[] ancestor;
    private ArrayList<Position> vertices;
    
    public Dijkstra(Map map, Position start) {
        initialize(map, start);
        ArrayList<Position> notYetUsed = new ArrayList(vertices);
        while(!notYetUsed.isEmpty()){ 
            Position u = notYetUsed.get(0);
            for(Position pos : notYetUsed){
                if(distance[vertices.indexOf(pos)] < distance[vertices.indexOf(u)]){
                    u = pos;
                }
            }
            notYetUsed.remove(u);
            if(notYetUsed.contains(new Position(u.x-1, u.y, u.z))){
                distanceUpdate(map, u, new Position(u.x-1, u.y, u.z));
            }
            if(notYetUsed.contains(new Position(u.x+1, u.y, u.z))){
                distanceUpdate(map, u, new Position(u.x+1, u.y, u.z));
            }
            if(notYetUsed.contains(new Position(u.x, u.y-1, u.z))){
                distanceUpdate(map, u, new Position(u.x, u.y-1, u.z));
            }
            if(notYetUsed.contains(new Position(u.x, u.y+1, u.z))){
                distanceUpdate(map, u, new Position(u.x, u.y+1, u.z));
            }
            if(notYetUsed.contains(new Position(u.x, u.y, u.z-1))){
                distanceUpdate(map, u, new Position(u.x, u.y, u.z-1));
            }
            if(notYetUsed.contains(new Position(u.x, u.y, u.z+1))){
                distanceUpdate(map, u, new Position(u.x, u.y, u.z+1));
            }
        }    
    }
    
    private void initialize(Map map, Position start) {
        vertices = map.getTraversablePositions();
        distance = new int[vertices.size()];
        ancestor = new Position[vertices.size()];
        for(int i = 0; i < vertices.size(); i++){
            distance[i] = Integer.MAX_VALUE;
            ancestor[i] = null;
        }
        distance[vertices.indexOf(start)] = 0;
    }
    
    private void distanceUpdate(Map map, Position u, Position v){       
        int alternative = distance[vertices.indexOf(u)] + map.getTile(v.z, v.x, v.y).getMovementCost();
        if(alternative < 0){
            alternative = Integer.MAX_VALUE;            
        }
        if(alternative < distance[vertices.indexOf(v)]){
            distance[vertices.indexOf(v)] = alternative;
            ancestor[vertices.indexOf(v)] = u;
        }
    }
    
    public ArrayList<Position> generatePath(Position aim){
        ArrayList<Position> path = new ArrayList();      
        if(vertices.indexOf(aim) == -1 || distance[vertices.indexOf(aim)] == Integer.MAX_VALUE){
            System.err.println("Kein Weg gefunden!"); // später entfernen
            return path;
        }
        path.add(aim);
        Position u = aim;
        while(distance[vertices.indexOf(ancestor[vertices.indexOf(u)])] != 0){
            u = ancestor[vertices.indexOf(u)];
            path.add(0, u);
        }
        return path;
    } 
}
