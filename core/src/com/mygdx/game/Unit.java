/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;
import java.util.ArrayList;

/**
 *
 * @author Alexander Hövelmann
 */
public abstract class Unit {
    public static final int UNIT_SIZE = 128;
    
    private final int maxHp;
    private int hp;
    private float viewAngleH, viewAngleV, maxViewDistance, eyeLevel;
    private Direction direction;
    private ArrayList<Position> path;
    private int rayNumberH, rayNumberV;
    private Position pos;
    
    public Unit(int maxHp, Position pos, int hp, Direction direction, float viewAngleH, float viewAngleV, float maxViewDistance, float eyeLevel) {
        this.maxHp = maxHp;
        this.pos = pos;
        this.hp = hp;
        this.direction = direction;
        this.viewAngleH = viewAngleH;
        this.viewAngleV = viewAngleV;
        this.maxViewDistance = maxViewDistance;
        this.eyeLevel = eyeLevel;
        
        //Temporär
        rayNumberH = (int)viewAngleH;
        rayNumberV = (int)viewAngleV;
    }
    
    public Unit(int maxHp, Position pos, int hp, Direction direction, float viewAngleH, float viewAngleV, float maxViewDistance, float eyeLevel, ArrayList<Position> path) {
        this(maxHp, pos, hp, direction, viewAngleH, viewAngleV, maxViewDistance, eyeLevel);
        this.path = path;
    }

    public float getViewAngleH() {
        return viewAngleH;
    }

    public float getViewAngleV() {
        return viewAngleV;
    }

    public float getMaxViewDistance() {
        return maxViewDistance;
    }

    public int getRayNumberH() {
        return rayNumberH;
    }

    public int getRayNumberV() {
        return rayNumberV;
    }    
    
    public Vector3 getTransformedPosition() {
        return pos.toVector3PixelTransformedPosition(0.5f, 0.5f, eyeLevel);//TEMP
    }
    
    public abstract Sprite getSprite();

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }    
    
    public Position getPosition() {
        return pos;
    }  
    
    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }  
}