/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author Alexander Hövelmann
 */
public enum Direction {
    NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST;
    
    public Vector2 toVector2(){
        Vector2 v2 = new Vector2(0, 1);
        float angle = (360f/Direction.values().length) * this.ordinal();
        return v2.rotate(angle);
    }
}
