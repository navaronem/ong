package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 *
 * @author Marvin Bongiovi
 */
public abstract class RayCluster {   
    public static Vector2 getCoordinate(Vector2 pos, int rayNumber, int ray, float distance) {
        Vector2 dir = new Vector2(1f, 0f);
        dir = dir.rotate(((float) ray) * (360f / ((float) rayNumber)));
        dir = dir.scl(distance);
        dir = dir.add(pos);
        return dir;
    }
    
    public static Vector3 getCoordinate(Vector3 pos, int rayNumber, int rayH, int rayV, float distance) {
        Vector3 dir = new Vector3(1f, 0f, 0f);
        dir.rotate(Vector3.X, ((float) rayV) * (360f / ((float) rayNumber)));
        dir.rotate(Vector3.Y, ((float) rayV) * (360f / ((float) rayNumber)));
        dir.rotate(Vector3.Z, ((float) rayH) * (360f / ((float) rayNumber)));
        dir = dir.scl(distance);
        dir = dir.add(pos);
        return dir;
    }
    
    public static Vector3 getViewCoordinate(Vector3 pos, Vector2 direction, float maxAngleH, float maxAngleV,int rayNumberH, int rayNumberV, int rayH, int rayV, float distance) {
        Vector3 dir = new Vector3(direction.x, direction.y, 0);       
        dir.rotate(Vector3.X, ((float) rayV) * (maxAngleV / ((float) rayNumberV))-maxAngleV/2);
        dir.rotate(Vector3.Y, ((float) rayV) * (maxAngleV / ((float) rayNumberV))-maxAngleV/2);
        dir.rotate(Vector3.Z, ((float) rayH) * (maxAngleH / ((float) rayNumberH))-maxAngleH/2);
        dir.scl(distance);
        dir.add(pos);
        return dir;
    }
}
